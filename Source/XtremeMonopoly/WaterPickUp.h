// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPickUps.h"
#include "WaterPickUp.generated.h"

/**
 * 
 */
UCLASS()
class XTREMEMONOPOLY_API AWaterPickUp : public AMyPickUps
{
	GENERATED_UCLASS_BODY()
	

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Water)
		float WaterLevel;

	void OnPickedUp_Implementation() override;
	
	
};
