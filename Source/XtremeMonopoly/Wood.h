

#pragma once


#include "GameFramework/Actor.h"
#include "MyPickUps.h"
#include "Wood.generated.h"


UCLASS()
class XTREMEMONOPOLY_API AWood : public AActor
{
	GENERATED_UCLASS_BODY()
public:

	AMyPickUps* CreateWood;

};
