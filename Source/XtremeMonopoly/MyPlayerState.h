// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "MyPlayerState.generated.h"

/**
 *
 */
UCLASS()
class XTREMEMONOPOLY_API AMyPlayerState : public APlayerState
{
	GENERATED_BODY()
		//AMyPlayerState();
public:
		UFUNCTION()
		bool AllItemsCollected();

};
