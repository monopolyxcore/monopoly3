// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */

class XTREMEMONOPOLY_API StateMachine
{
public:


	StateMachine();
	~StateMachine();
	
	int state;

	void OnChangeState(int n)
	{
		OnExit();
		state = n;
		OnEnter();
	}


	virtual void OnEnter() = 0;
	virtual void Update() = 0;
	virtual void OnExit() = 0;


};
