// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "StateMachine.h"
#include "XtremeMonopolyHUD.h"
/**
 * 
 */
class StateMachine;

class XTREMEMONOPOLY_API GameMenu:public StateMachine
{
public:

	AXtremeMonopolyHUD *hud;

	GameMenu();
	GameMenu(AXtremeMonopolyHUD *screen);
	~GameMenu();

	virtual void OnEnter();
	virtual void Update();
	virtual void OnExit();

};

/*
ChildSlot
.HAlign(HAlign_Center)
.VAlign(VAlign_Center)
[
SNew(SButton)
[
SNew(STextBlock)
.Text(FString("PLAY"))
]
]
*/