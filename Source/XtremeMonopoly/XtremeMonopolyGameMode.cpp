

#include "XtremeMonopoly.h"
#include "XtremeMonopolyGameMode.h"
#include "XtremeMonopolyPlayerController.h"
#include "MyPlayerState.h"

AXtremeMonopolyGameMode::AXtremeMonopolyGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnOb(TEXT("/Game/MixamoAnimPack/Mixamo_Kachujin/Mixamo_Kachujin"));
	DefaultPawnClass = PlayerPawnOb.Class;

	PlayerControllerClass = AXtremeMonopolyPlayerController::StaticClass();
	PlayerStateClass = AMyPlayerState::StaticClass();

}

void AXtremeMonopolyGameMode::PostRegisterAllComponents()
{
	//GameStateClass = AMyGameState::StaticClass();
}

void AXtremeMonopolyGameMode::SetCurrentState(EXtremeMonopolyPlayState NewState)
{
	CurrentState = NewState;

	HandleNewState(NewState);
}

void AXtremeMonopolyGameMode::HandleNewState(EXtremeMonopolyPlayState NewState)
{
	switch(NewState)
	{
	case EXtremeMonopolyPlayState::EMainMenu:
		break;
	case EXtremeMonopolyPlayState::EPlayingGame:
		break;
	case EXtremeMonopolyPlayState::EGameOver:
		break;
	case EXtremeMonopolyPlayState::EUnknown:
		break;
	default:
		break;
	}
}

void AXtremeMonopolyGameMode::CheckForWin()
{
	for (FConstControllerIterator It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AXtremeMonopolyPlayerController* PlayerController = Cast<AXtremeMonopolyPlayerController>(*It);
		if (PlayerController != nullptr)
		{
			AMyPlayerState* PlayerState = Cast<AMyPlayerState>((*It)->PlayerState);
			if (PlayerState != nullptr)
			{
				if (PlayerState->AllItemsCollected())
					GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Blue, TEXT("WIN"));
				// Passing true to bFocus here ensures that focus is returned to the game viewport.
				//ShooterHUD->ShowScoreboard(false, true);
			}
		}
	}
}

void AXtremeMonopolyGameMode::BeginPlay()
{
	Super::BeginPlay();
	CurrentState = EXtremeMonopolyPlayState::EMainMenu;
}

void AXtremeMonopolyGameMode::Tick(float DeltaTime)
{
	switch(CurrentState)
	{
	case EXtremeMonopolyPlayState::EMainMenu:
		//GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Blue, TEXT("STATE"));
		break;
	default:
		break;
	}
}

void AXtremeMonopolyGameMode::EndMatch()
{
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Black, TEXT("Oh Yeah WIN"));
}