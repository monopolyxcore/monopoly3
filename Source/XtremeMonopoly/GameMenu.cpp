// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "GameMenu.h"
#include "UITexture.h"
#include "UIText.h"

GameMenu::GameMenu() :hud(NULL)
{
}

GameMenu::GameMenu(AXtremeMonopolyHUD *screen)
{
	hud = screen;
}

GameMenu::~GameMenu()
{
}
void GameMenu::OnEnter()
{

}

void GameMenu::Update()
{
	UITexture resIcon;
	UIText TextResume;
	UIText TextOptions;
	UIText TextQuit;
	UIElement Buttons[3];

	if (hud)
	{
		switch (state)
		{
		case 0:
			resIcon = UITexture();
			resIcon.OnChangeState(1);
			break;
		case 1:
			for (int i = 0; i < 3; i++)
			{
				Buttons[i] = UIElement(FVector2D(hud->ScreenDimensions.X/2, hud->ScreenDimensions.Y /4 *(i+1)), FVector2D(250, 50), FColor::Blue, hud);
				Buttons[i].OnChangeState(1);
			}
			TextResume = UIText(FVector2D(hud->ScreenDimensions.X /2, hud->ScreenDimensions.Y / 4), FVector2D(125, 25), FColor::White, FString::Printf(TEXT("RESUME")), hud);
			TextResume.OnChangeState(1);
			hud->GetTextSize(TextResume.text, TextResume.scl.X, TextResume.scl.Y);
			TextOptions = UIText(FVector2D(hud->ScreenDimensions.X/2, hud->ScreenDimensions.Y / 2), FVector2D(125, 25), FColor::White, FString::Printf(TEXT("OPTIONS")), hud);
			TextOptions.OnChangeState(1);
			hud->GetTextSize(TextOptions.text, TextOptions.scl.X, TextOptions.scl.Y);
			TextQuit = UIText(FVector2D(hud->ScreenDimensions.X/2, hud->ScreenDimensions.Y / 4 * 3), FVector2D(125, 25), FColor::White, FString::Printf(TEXT("QUIT")), hud);
			TextQuit.OnChangeState(1);
			hud->GetTextSize(TextQuit.text, TextQuit.scl.X, TextQuit.scl.Y);
			break;
		case 2:

			break;
		}
	}

	resIcon.Update();
	for (int i = 0; i < 3; i++)
	{
		Buttons[i].Update();
	}
	TextResume.Update();
	TextOptions.Update();
	TextQuit.Update();
}

void GameMenu::OnExit()
{

}