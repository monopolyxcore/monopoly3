// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "WaterPickUp.h"


AWaterPickUp::AWaterPickUp(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	WaterLevel = 0.f;
}

void AWaterPickUp::OnPickedUp_Implementation()
{
	Super::OnPickedUp_Implementation();
	Destroy();
}