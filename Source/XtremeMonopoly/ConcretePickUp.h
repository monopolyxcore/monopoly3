// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPickUps.h"
#include "ConcretePickUp.generated.h"

/**
 * 
 */
UCLASS()
class XTREMEMONOPOLY_API AConcretePickUp : public AMyPickUps
{
	GENERATED_UCLASS_BODY()
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Concrete)
		float ConcreteLevel;

	void OnPickedUp_Implementation() override;
	
	
};
