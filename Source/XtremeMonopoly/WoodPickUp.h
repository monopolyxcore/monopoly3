// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPickUps.h"
#include "WoodPickUp.generated.h"

/**
 * 
 */
UCLASS()
class XTREMEMONOPOLY_API AWoodPickUp : public AMyPickUps
{
	GENERATED_UCLASS_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Wood)
		float WoodLevel;
		
	void OnPickedUp_Implementation() override;
};
