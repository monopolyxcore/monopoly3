// Fill out your copyright notice in the Description page of Project Settings.

#include "XtremeMonopoly.h"
#include "WoodPickUp.h"


AWoodPickUp::AWoodPickUp(const class FPostConstructInitializeProperties& PCIP) : Super(PCIP)
{
	WoodLevel = 0.f;
}

void AWoodPickUp::OnPickedUp_Implementation()
{
	Super::OnPickedUp_Implementation();
	Destroy();
}