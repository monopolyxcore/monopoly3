

#pragma once

#include "GameFramework/GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "XtremeMonopolyGameMode.generated.h"

UENUM()
enum EXtremeMonopolyPlayState
{
	EMainMenu UMETA(DisplayName = "Main Menu"),
	EPlayingGame UMETA(DisplayName = "Playing Game"),
	EGameOver UMETA(DisplayName = "Game Over"),
	EUnknown UMETA(DisplayName = "Unknown")
};

/**
*
*/
UCLASS()
class XTREMEMONOPOLY_API AXtremeMonopolyGameMode : public AGameMode
{
	GENERATED_BODY()
		AXtremeMonopolyGameMode(const class FPostConstructInitializeProperties& PCIP);
		virtual void BeginPlay() override;
	/*
	virtual void InitGame(
	const FString & MapName,
	const FString & Options,
	FString & ErrorMessage
	) override;

	virtual void InitGameState() overide;
	*/
	virtual void PostRegisterAllComponents() override;
	virtual void Tick(float DeltaTime) override;
	EXtremeMonopolyPlayState GetCurrentState() const;

	UFUNCTION()
		void SetCurrentState(EXtremeMonopolyPlayState NewState);
public:
	UFUNCTION()
		void CheckForWin();

	UFUNCTION(BlueprintCallable, Category = "Monopoly")
		virtual void EndMatch() override;

private:
	TEnumAsByte<EXtremeMonopolyPlayState> CurrentState;
	void HandleNewState(EXtremeMonopolyPlayState NewState);
};

FORCEINLINE EXtremeMonopolyPlayState AXtremeMonopolyGameMode::GetCurrentState() const
{
	return CurrentState;
}

//what?