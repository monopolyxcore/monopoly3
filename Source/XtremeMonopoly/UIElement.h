// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "StateMachine.h"
#include "XtremeMonopolyHUD.h"
/**
 * 
 */
class StateMachine;

class XTREMEMONOPOLY_API UIElement:public StateMachine
{
public:

	FVector2D centerpos, scl;
	FColor color;

	AXtremeMonopolyHUD *hud;

	UIElement();
	UIElement(FVector2D position, FVector2D size, FColor col, AXtremeMonopolyHUD *Screen);

	~UIElement();

	virtual void OnEnter();
	virtual void Update();
	virtual void OnExit();

	FVector2D GetStartDrawPos();

	bool OnBox(FVector2D cursor)
	{
		if ((cursor.X >= GetStartDrawPos().X) && (cursor.X <= GetStartDrawPos().X + scl.X) && (cursor.Y >= GetStartDrawPos().Y) && (cursor.Y <= GetStartDrawPos().Y + scl.Y))
		{
			return true;
		}
		return false;
	}

};
